#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import random
import requests

''' Get random image from tumblr
    - get random post at http://anyblog.tumblr.com/random
    - get image HD image of post

'''

page_list = [
            "https://mrtux.tumblr.com",
            "http://aodaixinh.tumblr.com"
            ]
retry_count = 5


def getImg():
    r = requests.get(random.choice(page_list) + "/random")
    try:
        imgTag = re.search(r'<a href="(.*?)" class="zoom"', r.text).group(0)
        img = re.search(r'href="(.*?)\?.jpg"', imgTag).group(1)
    except:
        imgTag = re.search(r'<img(.*?)class="permalinkPrimaryPhoto"(.*?)',
                           r.text).group(0)
        img = re.search(r'src="(.*?)"', imgTag).group(1)

    return img


def getRandomImg():
    retry = 0
    while retry < retry_count:
        try:
            return getImg()
        except:
            retry += 1
            pass

    return u"Tao ghét. Tao éo post nữa!"
