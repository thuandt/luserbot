#!/usr/bin/python
# -*- coding: utf-8 -*-
from telegram import Updater
from plugins import xkcn
import keyring
import logging

# Enable Logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

# get token from keyring
botToken = keyring.get_password('telegram', 'luser_bot')
updater = Updater(token=botToken)
dispatcher = updater.dispatcher
print updater.bot.getMe()
# updater.bot.sendMessage(chat_id=receiver, text=content)


def girl(bot, update):
    imgUrl = xkcn.getRandomImg()
    bot.sendMessage(chat_id=update.message.chat_id, text=imgUrl)

dispatcher.addTelegramCommandHandler('girl', girl)
updater.start_polling(poll_interval=0.1, timeout=10)
updater.idle()
